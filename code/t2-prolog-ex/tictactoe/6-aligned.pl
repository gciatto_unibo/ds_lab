cell(1, 1, o).
cell(1, 2, e).
cell(1, 3, x).    %%%%%%%%% 
cell(2, 1, e).    % o _ _ % 
cell(2, 2, x).    % _ x _ %
cell(2, 3, e).    % x _ _ %
cell(3, 1, e).    %%%%%%%%%
cell(3, 2, e).
cell(3, 3, e).

% aligned(?Symbols, ?Cells)
aligned(Symbols, Cells) :-
  pattern(Cells, Symbols),
  line(Cells).
  
% 1) Try the following queries (keep accepting solutions until no more are proposed):
%
% ?- aligned([S, S, S], Cells).
% ?- aligned([o, S1, S2], Cells).
% ?- aligned([x, x, e], [_, _, cell(X, Y, _)]).
% ?- aligned([S, S, P], Cells), S \== P.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation details %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% pattern(?Cells, ?Symbols)
pattern([], []).
pattern([cell(_, _, S) | OtherCells], [S | OtherSymbols]) :-
  pattern(OtherCells , OtherSymbols).

% line(?Cells)
line(Cells) :- east(Cells).
line(Cells) :- west(Cells).
line(Cells) :- north(Cells).
line(Cells) :- south(Cells).
line(Cells) :- north_east(Cells).
line(Cells) :- north_west(Cells).
line(Cells) :- south_east(Cells).
line(Cells) :- south_west(Cells).

east(cell(X1, Y, E1), cell(X2, Y, E2)) :- cell(X1, Y, E1), cell(X2, Y, E2), 1 is X1 - X2.
east([C1, C2]) :- east(C1, C2).
east([C1, C2 | Cs]) :- east(C1, C2), east([C2 | Cs]).

west(cell(X1, Y, E1), cell(X2, Y, E2)) :- cell(X1, Y, E1), cell(X2, Y, E2), 1 is X2 - X1.
west([C1, C2]) :- west(C1, C2).
west([C1, C2 | Cs]) :- west(C1, C2), west([C2 | Cs]).

north(cell(X, Y1, E1), cell(X, Y2, E2)) :- cell(X, Y1, E1), cell(X, Y2, E2), 1 is Y2 - Y1.
north([C1, C2]) :- north(C1, C2).
north([C1, C2 | Cs]) :- north(C1, C2), north([C2 | Cs]).

south(cell(X, Y1, E1), cell(X, Y2, E2)) :- cell(X, Y1, E1), cell(X, Y2, E2), 1 is Y1 - Y2.
south([C1, C2]) :- south(C1, C2).
south([C1, C2 | Cs]) :- south(C1, C2), north([C2 | Cs]).

north_east(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y2 - Y1, 1 is X1 - X2.
north_east([C1, C2]) :- north_east(C1, C2).
north_east([C1, C2 | Cs]) :- north_east(C1, C2), north_east([C2 | Cs]).

north_west(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y2 - Y1, 1 is X2 - X1.
north_west([C1, C2]) :- north_west(C1, C2).
north_west([C1, C2 | Cs]) :- north_west(C1, C2), north_west([C2 | Cs]).

south_east(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y1 - Y2, 1 is X1 - X2.
south_east([C1, C2]) :- south_east(C1, C2).
south_east([C1, C2 | Cs]) :- south_east(C1, C2), south_east([C2 | Cs]).

south_west(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y1 - Y2, 1 is X2 - X1.
south_west([C1, C2]) :- south_west(C1, C2).
south_west([C1, C2 | Cs]) :- south_west(C1, C2), south_west([C2 | Cs]).
