turn(o).

cell(1, 1, o).
cell(1, 2, e).
cell(1, 3, x).    %%%%%%%%% 
cell(2, 1, e).    % o _ _ % 
cell(2, 2, x).    % _ x _ %
cell(2, 3, e).    % x _ _ %
cell(3, 1, e).    %%%%%%%%%
cell(3, 2, e).
cell(3, 3, e).

% move (+X, +Y, +S)
move(X, Y, S) :-
  turn(S),
  retract(cell(X, Y, e)),
  asserta(cell(X, Y, S)),
  next_turn(S, NS),
  retract(turn(S)),
  asserta(turn(NS)).
  
% next_turn(?S1, ?S2)
next_turn(x, o).
next_turn(o, x).

% 1) Try the following queries (keep accepting solutions until no more are proposed):
%
% ?- move(2, 2, o).
% revert your side-effects resetting your theory (Set Theory Button)
% ?- move(1, 1, o).
% revert your side-effects resetting your theory (Set Theory Button)
% ?- move(3, 1, o).
% revert your side-effects resetting your theory (Set Theory Button)
% ?- move(3, 1, o), move(3, 1, o).
% which is the state of your knowledge base now? Get your theory to inspect it (Get Theory button)
% revert your side-effects resetting your theory (Set Theory Button)
% ?- move(3, 1, o), move(3, 1, o).
% DO NOT revert your side-effects
% ?- move(2, 1, x). 
