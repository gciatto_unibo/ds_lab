turn(x).

cell(1, 1, e).
cell(1, 2, e).
cell(1, 3, e).    %%%%%%%%% 
cell(2, 1, e).    % _ _ _ % 
cell(2, 2, e).    % _ _ _ %
cell(2, 3, e).    % _ _ _ %
cell(3, 1, e).    %%%%%%%%%
cell(3, 2, e).
cell(3, 3, e).

% move (+X, +Y, +S)
move(X, Y, S) :-
  turn(S),
  retract(cell(X, Y, e)),
  asserta(cell(X, Y, S)),
  next_turn(S, NS),
  retract(turn(S)),
  asserta(turn(NS)).

% aligned(?Symbols, ?Cells)
aligned(Symbols, Cells) :-
  pattern(Cells, Symbols),
  line(Cells).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation details %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% next_turn(?S1, ?S2)
next_turn(x, o).
next_turn(o, x).

% pattern(?Cells, ?Symbols)
pattern([], []).
pattern([cell(_, _, S) | OtherCells], [S | OtherSymbols]) :-
  pattern(OtherCells , OtherSymbols).

east(cell(X1, Y, E1), cell(X2, Y, E2)) :- cell(X1, Y, E1), cell(X2, Y, E2), 1 is X1 - X2.

west(cell(X1, Y, E1), cell(X2, Y, E2)) :- cell(X1, Y, E1), cell(X2, Y, E2), 1 is X2 - X1.

north(cell(X, Y1, E1), cell(X, Y2, E2)) :- cell(X, Y1, E1), cell(X, Y2, E2), 1 is Y2 - Y1.

south(cell(X, Y1, E1), cell(X, Y2, E2)) :- cell(X, Y1, E1), cell(X, Y2, E2), 1 is Y1 - Y2.

north_east(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y2 - Y1, 1 is X1 - X2.

north_west(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y2 - Y1, 1 is X2 - X1.

south_east(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y1 - Y2, 1 is X1 - X2.

south_west(cell(X1, Y1, E1), cell(X2, Y2, E2)) :- cell(X1, Y1, E1), cell(X2, Y2, E2), 1 is Y1 - Y2, 1 is X2 - X1.

:- include('10-metalib.pl').

:- initialization((
	write("Initalization... "),
	Directions = [east, south, north, south, north_east, north_west, south_east, south_west],
	binary_to_nary(Directions),
	disjunct(line, Directions),
	write("OK!"), nl
)).
