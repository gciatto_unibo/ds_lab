a(X) :- write(a), nl, X = 1.
b(X) :- write(b), nl, X = 2.
c(X) :- write(c), nl, X = 3.

val(X) :- a(X).
val(X) :- b(X).
val(X) :- c(X).

% 1) Try the following queries (keep accepting solutions until no more are proposed):
%
% ?- not val(1).
% ?- not val(2).
% ?- not val(3).
% ?- not val(4).