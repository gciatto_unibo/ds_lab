turn(o).

cell(1, 1, o).
cell(1, 2, e).
cell(1, 3, x).    %%%%%%%%% 
cell(2, 1, e).    % o _ _ % 
cell(2, 2, x).    % _ x _ %
cell(2, 3, e).    % x _ _ %
cell(3, 1, e).    %%%%%%%%%
cell(3, 2, e).
cell(3, 3, e).

% 1) Try the following queries (keep accepting solutions until no more are proposed):
% 2) Try to "translate" each query into natural language, assuming *this particular representation*
%
% ?- cell(X, Y, x). % Give me a cell (X, Y) contanining an 'x'
% ?- cell(X, Y, e). % <natural language here>
% ?- cell(X, Y, S), S \= e. % <natural language here>
% ?- cell(2, 3, S). % <natural language here> 