cell(1, 1, o).
cell(1, 2, e).
cell(1, 3, x).    %%%%%%%%% 
cell(2, 1, e).    % o _ _ % 
cell(2, 2, x).    % _ x _ %
cell(2, 3, e).    % x _ _ %
cell(3, 1, e).    %%%%%%%%%
cell(3, 2, e).
cell(3, 3, e).

% north(?Cell1, ?Cell2)
north(cell(X, Y1, E1), cell(X, Y2, E2)) :- 
  cell(X, Y1, E1), % what's the purpose of this sub-goal?
  cell(X, Y2, E2), % what's the purpose of this sub-goal?
  1 is Y2 - Y1.
  
% north(?Cells:list)
north([C1, C2]) :- 
  north(C1, C2).
north([C1, C2 | Cs]) :- 
  north(C1, C2), north([C2 | Cs]).
  
% 1) Try the following queries (keep accepting solutions until no more are proposed):
%
% ?- north(cell(1, 1, _), cell(1, 2, _)).
% ?- north(cell(2, 1, _), cell(2, 3, _)).
% ?- north(cell(2, 1, _), Cell2).
% ?- north(Cell1, cell(1, 2, _)).
% ?- north(Cell1, Cell2).
% ?- north([cell(1, 1, _), cell(1, 2, _), cell(1, 3, _)]).
% ?- north([C1, C2, C3]).
% ?- north(L).
%
% 2) Retry all queries after removing sub-goals from lines 13 and 14.