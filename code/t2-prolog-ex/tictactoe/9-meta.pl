binary_to_nary(Op) :-
  % op([X1, X2]) :- op(X1, X2).
  BaseCaseHead =.. [Op, [X, Y]],
  BaseCaseBody =.. [Op, X, Y],
  assertz(BaseCaseHead :- BaseCaseBody),
  % op([X1, X2 | Xs]) :- op(X1, X2), op([X2 | Xs]).
  RecursiveCaseHead =.. [Op, [X1, X2 | Xs]],
  RecursiveCaseBody1 =.. [Op, X1, X2],
  RecursiveCaseBody2 =.. [Op, [X2 | Xs]],
  assertz(RecursiveCaseHead :- (RecursiveCaseBody1, RecursiveCaseBody2)).
  
greater_than(X, Y) :- X > Y.

first_gaol :-
    Head =.. [sum, X, Y, R],
    Body = 'is'(R, '+'(X, Y)),
    Clause = ':-'(Head, Body),
    assert(Clause),
    binary_to_nary(greater_than),
    write("Successfully initialized!"), nl.
    
:- initialization(first_gaol).

% 1) Try the following queries (keep accepting solutions until no more are proposed):
%
% ?- sum(3, 4, R).
% ?- greater_than(3, 2).
% ?- greater_than([4, 3, 2, 1]).
%
% 2) Now comment the initialization directive (line 8), (re)set your theory and retry.