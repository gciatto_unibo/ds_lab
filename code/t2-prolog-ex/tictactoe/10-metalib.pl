binary_to_nary(Op) :-
  % op([X1, X2]) :- op(X1, X2).
  BaseCaseHead =.. [Op, [X, Y]],
  BaseCaseBody =.. [Op, X, Y],
  assertz(BaseCaseHead :- BaseCaseBody),
  % op([X1, X2 | Xs]) :- op(X1, X2), op([X2 | Xs]).
  RecursiveCaseHead =.. [Op, [X1, X2 | Xs]],
  RecursiveCaseBody1 =.. [Op, X1, X2],
  RecursiveCaseBody2 =.. [Op, [X2 | Xs]],
  assertz(RecursiveCaseHead :- (RecursiveCaseBody1, RecursiveCaseBody2)).
  
binary_to_nary([]).
binary_to_nary([OP | OPs]) :- 
  binary_to_nary(OP),
  binary_to_nary(OPs).

alias(Op, Alias) :-
  % op(X) :- alias(X).
  ClauseHead =.. [Op, X],
  ClauseBody =.. [Alias, X],
  assertz(ClauseHead :- ClauseBody).
  
disjunct(_, []).
disjunct(Op, [A | As]) :-
  alias(Op, A),
  disjunct(Op, As).

:- initialization((write("metalib loaded!"), nl)).