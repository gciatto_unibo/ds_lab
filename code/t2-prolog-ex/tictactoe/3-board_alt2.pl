turn(o).
                  %%%%%%%%%
cell(1, 1, o).    % o _ _ % 
cell(1, 3, x).    % _ x _ %
cell(2, 2, x).    % x _ _ %
                  %%%%%%%%%
   
% 1) Try the following queries (keep accepting solutions until no more are proposed):
% 2) Try to "translate" each query into natural language, assuming *this particular representation*
%
% ?- cell(X, Y, x). % <natural language here>
% ?- cell(X, Y, e). % <natural language here>
% ?- cell(X, Y, S), S \= e. % <natural language here>
% ?- cell(2, 3, S). % <natural language here> 
%
% 3) Try to answer the following questions:
% 
% - How would you check if a particual cell is empty?
% ?- <prolog here>.
% - How would you check if a particual cell contains symbol S?
% ?- <prolog here>.
% - How would you iterate over empty (resp. non-empty) cells?
% ?- <prolog here (empty)>.
% ?- <prolog here (non-empty)>.
