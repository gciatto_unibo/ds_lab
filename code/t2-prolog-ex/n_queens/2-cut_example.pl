a(1). 
b(2). 
c(3).

val(X) :- a(X).
val(X) :- b(X)/* , !*/.
val(X) :- c(X).

% Try the following queries
% (keep accepting more solution if proposed)
%
% ?- val(X).
%
% Now remove the comments from line 6
% Retry all queries
% What changes?