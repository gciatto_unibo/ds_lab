% range(+First:int, +Last:int, -Range:list)
range(Last, Last, [Last]) :- !.
range(First, Last, [First | Others]) :-
  First < Last,
  Next is First + 1,
  range(Next, Last, Others).
  
% noattack(+Cell1:couple, +Cell2:couple)
noattack((X1, Y1), (X2, Y2)) :- !,
  X1 =\= X2,
  Y1 =\= Y2,
  (Y2 - Y1) =\= (X2 - X1),
  (Y2 - Y1) =\= (X1 - X2).

% noattack(+Cell1:couple, +Cells:list)
noattack(_, []).
noattack(Cell , [HeadCell | Others]) :-
  noattack(Cell, HeadCell ),
  noattack(Cell, Others ).
  
% solution (+NumOfQueens:int, Template:list)
solution(_, []).
solution(NumOfQueens, [(X, Y) | Template]) :-
  solution(NumOfQueens, Template),
  range(1, NumOfQueens, Range), % Range = [1..NumOfQueens]
  member(Y, Range),
  noattack((X, Y), Template).

% template(+Nums:list, Cells:list)
template([], []). 
template([X | Xs], [(X, _) | XYs]) :-
  template(Xs, XYs).
  
% template(+NumOfQueens:int, Template:list)
template(NumOfQueens, Template) :-
  range(1, NumOfQueens, Range),
  template(Range, Template).
  
% n_queens(+NumOfQueens:int, Solution:list)
n_queens(NumOfQueens, Solution) :-
  template(NumOfQueens, Solution),
  solution(NumOfQueens, Solution).
  
% Try the following queries
%
% ?- n_queens(1, S).
% ?- n_queens(2, S).
% ?- n_queens(3, S).
% ?- n_queens(4, S).
% ?- n_queens(8, S).
% ?- n_queens(16, S). 

