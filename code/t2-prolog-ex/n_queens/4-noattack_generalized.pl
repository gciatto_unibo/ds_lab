% noattack(+Cell1:couple, +Cell2:couple)
noattack((X1, Y1), (X2, Y2)) :- !, % which is the purpose of the cut here?
  X1 =\= X2,
  Y1 =\= Y2,
  (Y2 - Y1) =\= (X2 - X1),
  (Y2 - Y1) =\= (X1 - X2).

% noattack(+Cell1:couple, +Cells:list)
noattack(_, []).
noattack(Cell , [HeadCell | Others]) :- % where 's recursion ?
  noattack(Cell , HeadCell), % which one is called here ?
  noattack(Cell , Others). % which one is called here ?
  
% Try the following queries
%
% ?- noattack((1,3), [(2,1), (3,4), (4,2)]).
% ?- noattack((4,4), [(2,2), (3,4), (4,2)]).