% noattack(+Cell1:couple, +Cell2:couple)
noattack((X1, Y1), (X2, Y2)) :- /* !, % which is the purpose of the cut here? */
  X1 =\= X2, % not same column
  Y1 =\= Y2, % not same row
  (Y2 - Y1) =\= (X2 - X1), % not same diagonal
  (Y2 - Y1) =\= (X1 - X2). % not same anti-diagonal
  
% Try the following queries
%
% ?- noattack((1, 1), (2, 3)).
% ?- noattack((3, 4), (3, 7)).
% ?- noattack((7, 3), (3, 7)).
% ?- noattack((1, 7), (3, 7)).
% ?- noattack((1, 1), [2, 3]).
% ?- noattack((a, 7), (2, c)).
% ?- noattack((a, 7), (2, c)).
% ?- noattack((2, 2), (X, Y)).
% ?- noattack((2, 2), Cell).
