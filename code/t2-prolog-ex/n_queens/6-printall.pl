lprintall([]) .
lprintall([X | Xs]) :-
  write(X), 
  nl,
  lprintall(Xs).
  
rprintall([]) .
  rprintall([X | Xs]) :-
  rprintall(Xs),
  write(X), nl.
  
% Try the following queries
%
% ?- lprintall([a, b, c, d, e, f]).
% ?- rprintall([a, b, c, d, e, f]).