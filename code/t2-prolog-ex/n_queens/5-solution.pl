% range(+First:int, +Last:int, -Range:list)
range(Last, Last, [Last]) :- !.
range(First, Last, [First | Others]) :-
  First < Last,
  Next is First + 1,
  range(Next, Last, Others).
  
% noattack(+Cell1:couple, +Cell2:couple)
noattack((X1, Y1), (X2, Y2)) :- !,
  X1 =\= X2,
  Y1 =\= Y2,
  (Y2 - Y1) =\= (X2 - X1),
  (Y2 - Y1) =\= (X1 - X2).

% noattack(+Cell1:couple, +Cells:list)
noattack(_, []).
noattack(Cell , [HeadCell | Others]) :-
  noattack(Cell, HeadCell ),
  noattack(Cell, Others ).
  
% solution (+NumOfQueens:int, Template:list)
solution(_, []).
solution(NumOfQueens, [(X, Y) | Template]) :-
  solution(NumOfQueens, Template),
  range(1, NumOfQueens, Range), % Range = [1..NumOfQueens]
  member(Y, Range),
  noattack((X, Y), Template).
  
% Try the following queries
% (keep accepting more solution for further solutions)
%
% ?- solution(1, [(1, Y1)]).
% ?- solution(2, [(1, Y1), (2, Y2)]).
% ?- solution(3, [(1, Y1), (2, Y2), (3, Y3)]).
% ?- solution(4, [(1, Y1), (2, Y2), (3, Y3), (4, Y4)]).
% ?- solution(8, [(1, Y1), (2, Y2), (3, Y3), (4, Y4), (5, Y5), (6, Y6), (7, Y7), (8, Y8)]).