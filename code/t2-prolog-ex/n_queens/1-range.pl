% range(+First:int, +Last:int, -Range:list)
range(Last, Last, [Last]). /* :- !. */
range(First, Last, [First | Others]) :-
  First < Last,
  Next is First + 1,
  range(Next, Last, Others).

  
% Try the following queries
% (keep accepting more solution if proposed)
%
% ?- range(1, 5, L).
% ?- range(-1, 2, [-1, 0, 1, 2]).
% ?- range(6, 6, L).
% ?- range(7, 6, _).
%
% Now remove the comments from line 2
% Retry all queries
% What changes?