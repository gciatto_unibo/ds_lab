% range(+First:int, +Last:int, -Range:list)
range(Last, Last, [Last]) :- !.
range(First, Last, [First | Others]) :-
  First < Last,
  Next is First + 1,
  range(Next, Last, Others).

% template(+Nums:list, Cells:list)
template([], []). 
template([X | Xs], [(X, _) | XYs]) :-
  template(Xs, XYs).

% template(+NumOfQueens:int, Template:list)
template(NumOfQueens, Template) :-
  range(1, NumOfQueens, Range),
  template(Range, Template).
  
% Try the following queries
%
% ?- template(1, L).
% ?- template(2, L).
% ?- template(4, L).
% ?- template(8, L).

