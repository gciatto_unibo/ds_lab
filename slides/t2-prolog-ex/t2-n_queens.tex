%===============================================================================
\section{$N$-Queens}
%===============================================================================
\fr{\prolog{} Example \#1: $N$-Queens}{
	\bl{Brief description of the $N$-Queens game}{
  		We want to dispose $N$ queens\footnote{A piece from the well known ``Chess'' strategy game. A queen can attack any other piece being on the same row, column, diagonal and anti-diagonal} on a $N \times N$ chessboard in such a way that no queen is able to attack the other ones.
	}
	\begin{exampleblock}{A solution to the $8$-Queens problem}
		A solution to the $N$-Queens problem is commonly represented as a $N$-uple of column-row (or X-Y) coordinates representing the queens'positions on the chessboard. For instance:
		\[(1,4),\ (2,2),\ (3,7),\ (4,3),\ (5,6),\ (6,8),\ (7,5),\ (8,1)\]
	\end{exampleblock}
}

%-------------------------------------------------------------------------------
\subsection{Thinking about Goals}
%-------------------------------------------------------------------------------
\fr{Informal Decomposition of the Problem}{
	\begin{itemize}
		\item we expect the solution to be a \emph{list} of coordinates representing queens positions on the board:
		\iz{
			\item e.g., \cil{[(X1, X2), ..., (XN, YN)]}
			\item we need a \cil{n_queens(+N:int, -Cells:list)} predicate, iterating over \emph{columns} and assigning \emph{rows} to queens
		}
		\vfill
		\item we need to test whether some queen in position \cil{(X, Y)} may \emph{attack} another queen in position \cil{(X1, Y1)} or not
		\iz{
			\item we need a \cil{noattack(+Cell1, +Cell2)} predicate, checking if the first queen \emph{is not} able to attack the second one
			\item we may actually want to test this property against more than two queens\ldots{}
		}
		\vfill
		\item it may be useful to \emph{range} over numbers from $1$ to $N$
		\iz{
			\item we need a \cil{range(+Fst:int, +Lst:int, -Range:list)} predicate, \emph{generating} the list of numbers \cil{[Fst, Fst + 1, ..., Lst]}
		} 
	\end{itemize}
}

%-------------------------------------------------------------------------------
\subsection{Iteration \& Return Values}
%-------------------------------------------------------------------------------
\fr{The \cil{range/3} Predicate -- Try it Yourself}{
	\bl{Advices for \cil{range(+Fst:int, +Lst:int, -Range:list)}}{
		\iz {
			\item in the general case (\cil{Fst < Lst}), the following conditions should hold:
			\iz{
				\item the head of \cil{Range} should unify with \cil{Fst}
				\item the head of the tail of \cil{Nums} should unify with \cil{Fst + 1}
				\item the head of the tail of the tail of \cil{Nums} should unify with \cil{Fst + 2}
				\item \ldots
			}
			\item whenever \cil{Fst = Lst}, the returning list should unify with the singleton \cil{[Lst]}.
		}	
	}
	\ebl{\cil{range/3} usage examples}{
		\iz{
			\item \cil{?- range(1, 5, L).} $\longrightarrow$ \cil{L/[1, 2, 3, 4, 5].}
			\item \cil{?- range(-1, 2, [-1, 0, 1, 2]).} $\longrightarrow$ \cil{yes.}
			\item \cil{?- range(6, 6, L).} $\longrightarrow$ \cil{L/[6].}
			\item \cil{?- range(7, 6, _).} $\longrightarrow$ \cil{no.}
		}
	}
}

\fr{The \cil{range/3} Predicate -- Possible Solution}{
	\begin{columns}
		\begin{column}{0.39\linewidth}
			\ebl{Our proposal}{
				\lstinputlisting{./code/range.pl}	
			}
		\end{column}
		\hfill
		\begin{column}{0.58\linewidth}
%			\bl{Remarks}{
				\iz{
					\item try querying \cil{?- range(1, 5, L)}
					\iz{
						\item why is the engine proposing to search for alternative solutions?
						\item is the engine able to provide alternative solutions?
					}
					\item try replacing the \cil{is/2} operator with the \cil{'='/2} and see what changes.
					\item try removing the \cil{F < L} and see what changes.
					\item note that ``results'' are ``built'' by providing \emph{unbounded} variables to invoked predicates and letting them bind such variables to some partial result
				}
%			}
		\end{column}
	\end{columns}
}

%-------------------------------------------------------------------------------
\subsection{The cut Operator}
%-------------------------------------------------------------------------------
\fr{The \emph{cut} Operator: \cil{'!'/0}}{
	\bl{Informal definition}{
		The cut operator (\cil{'!'/0}) is a 0-arity atom always evaluating to \cil{true}. Its aim is to provide a powerful \emph{side-effect}. It prevents \emph{backtracking} to any un-explored branch of the proof tree prior to the cut evaluation. 
	}
	\ebl{Cut example 1}{
		Try replacing \cil{range(L, L, [L]) :- !.} to the non-recursive case of the \cil{range/3} predicate definition. What changes?
	}
	\ebl{Cut example 2}{
	\begin{columns}
		
		\begin{column}{0.38\linewidth}
			\lstinputlisting{./code/cut.pl}
		\end{column}
		\begin{column}{0.58\linewidth}
			\iz{
				\item try querying \cil{?- val(X)} and keep accepting until no more solution is provided. 
				\item now remove the comments and retry.
			}
		\end{column}
	\end{columns}
	}
}

%-------------------------------------------------------------------------------
\subsection{One-way Predicates}
%-------------------------------------------------------------------------------
\fr{The \cil{noattack/2} Predicate -- Try it Yourself}{
	\bl{Advices for \cil{noattack(+Cell1, +Cell2)}}{
		A queen in position \cil{Cell1 = (X1, Y1)} \emph{cannot attack} another queen in position \cil{Cell2 = (X2, Y2)} if \cil{Cell1} and \cil{Cell2}
		\iz {
			\item are not on the same \emph{row}
			\item are not on the same \emph{column} 
			\item are not on the same \emph{diagonal} 
			\item are not on the same \emph{anti-diagonal}
		}	
	}
	\ebl{\cil{noattack/2} usage examples}{
		\iz{
			\item \cil{?- noattack((1, 1), (2, 3)).} $\longrightarrow$ \cil{yes.}
			\item \cil{?- noattack((3, 4), (3, 7)).} $\longrightarrow$ \cil{no.}
			\item \cil{?- noattack((7, 3), (3, 7)).} $\longrightarrow$ \cil{no.}
			\item \cil{?- noattack((1, 7), (3, 7)).} $\longrightarrow$ \cil{no.}
		}
	}
}

\fr{The \cil{noattack/2} Predicate -- Possible Solution}{
	\ebl{Our proposal}{
		\lstinputlisting{./code/noattack.pl}	
	}
%	\bl{Remarks}{
		\iz{
			\item what if the actual arguments are not couples?
			\item what if the actual arguments are not couples \emph{of numbers}?
			\item what if the actual arguments contains one or more unbounded variables?
			\iz {
				\item e.g. what's the expected outcome of \cil{?- noattack((2,2),(X,Y)).}
				\item can this predicate be used to \emph{generate} a non-attacking couple of cells?
				\item why?
			}
		}
%	}

}

\fr{The \cil{noattack/2} Predicate -- Generalisation}{
	\bl{Try it yourself}{
		Try now to generalize the \cil{noattack/2} predicate to check the case where a queen in \cil{Cell = (X, Y)} is not able to attack any queen in positions \cil{Cells = [(X1,Y1), (X2,Y2), ...]}
	}
	
	\pause
	
	\ebl{Our proposal}{
		\lstinputlisting{./code/noattack_gen.pl}	
	}
}

%-------------------------------------------------------------------------------
\subsection{The Power of Backtracking}
%-------------------------------------------------------------------------------
\lfr{The \cil{solution/2} Predicate -- Try it Yourself}{
	
	Let's assume \cil{Template = [(X1, Y1), ..., (XN, YN)]}, where each \cil{Yi} is \emph{unbound} and all \cil{Xi} are assigned to all values in the set \cil{\{1..N\}}.
	\iz{
		\item we will enforce this assumption later
	}
	
	\bl{Advices for \cil{solution(+N:int, ?Template:list)}}{
		
		Under such hypotheses, a solution to the \cil{N}-Queens problem can be computed as follows
		\en{
			\item choose a queen-free column, i.e. choose some \cil{Xi} in \cil{Template} such that \cil{Yi} is still unassigned
			\item \label{nqueens.sol.selectY} select some value in \cil{\{1..N\}} and assign it to \cil{Yi}
			\item ensure queen \cil{(Xi, Yi)} is \emph{not} attacking any \emph{already located} one
%			\iz{
%				\item if it does, go back to step \ref{nqueens.sol.selectY}
%			}
			\item once all \cil{Yi} have been assigned, you're done
		}
	}

	\ebl{\cil{solution/2} usage examples}{
		\iz{
			\item \cil{?- solution(1, [(1, Y1)]).} $\longrightarrow$ \cil{Y1/1.}
			\item \cil{?- solution(2, [(1, Y1), (2, Y2)]).} $\longrightarrow$ \cil{no.}
			\item \cil{?- solution(3, [(1, Y1), (2, Y2), (3, Y3)]).} $\longrightarrow$ \cil{no.}
			\item \cil{?- solution(4, [(1, Y1), (2, Y2), (3, Y3), (4, Y4)]).} $\longrightarrow$ \cil{Y1/3, Y2/1, Y3/4, Y4/2.}
			\item \cil{?- solution(8, [(1, Y1), (2, Y2), (3, Y3), (4, Y4), (5, Y5), (6, Y6), (7, Y7), (8, Y8)]).} $\longrightarrow$ \cil{Y1/4, Y2/2, Y3/7, Y4/3, Y5/6, Y6/8, Y7/5, Y8/1}
		}
	}
}

\lfr{The \cil{solution/2} Predicate -- Possible Solution}{
	\begin{columns}
		\begin{column}{0.49\linewidth}
			\ebl{Our proposal}{
				\lstinputlisting{./code/solution.pl}
			}
		\end{column}
		\hfill
		\begin{column}{0.49\linewidth}
%			\bl{Remarks}{
				\iz {
					\item why is the recursive call appearing as the \emph{first} subgoal of \cil{solution/2}?
					\iz { \item what if it was the last call? }
					\item where are we choosing a ``queen-free'' column?
					\item where are we ensuring the current candidate queen is not attacking any ``already located'' one?
					\item isn't \cil{Y} unbounded? Where are we selecting a value for \cil{Y}?
					\item if the selected value is invalid, where are we selecting another one?
				}
%			}
		\end{column}
	\end{columns}
	
	\framebreak
	
	\bl{Why recursion is on first sub-goal? (informally)}{
		\iz{
			\item \prolog{} search strategy is \emph{depth-first}
			\item[$\rightarrow$] when iterating over the elements of some lists\ldots{}
			\iz{
				\item if recursion is on \emph{last} subgoal, elements are visited \emph{from left to right},
				\item if recursion is on \emph{first} subgoal, elements are visited \emph{from right to left}.
			}
		}	
	}
	
	\ebl{Try \cil{?- L = [a, b, c, d], lprintall(L), rprintall(L).}}{
		\begin{columns}
			\begin{column}{0.49\linewidth}
				\lstinputlisting{./code/left_recursion.pl}
			\end{column}
			\begin{column}{0.49\linewidth}
				\lstinputlisting{./code/right_recursion.pl}
			\end{column}
		\end{columns}
	}

	\framebreak
	
	\iz{
		\item we are actually visiting columns from right to left
		\item assigning \cil{Yi} from right to left
		\item[!] so \cil{T} --- the tail of the template --- always contains the ``already located'' queens\ldots{}
		\iz{ \item where ``always'' means ``in any iteration step of \cil{solution/2}''}
		\item \ldots{} thus it is sufficient to check that queen in \cil{(X, Y)} is not attacking any queen in \cil{T}.
	}

	\framebreak
	
	\bl{\cil{?- member(Y, [1, 2, ..., N]), Goal.}}{
		Assuming \cil{Y} is initially \emph{unbounded} and \cil{Goal} employs \cil{Y} somehow, this can be read as:
		
		\begin{center}
			\emph{Select a number in \{1..N\}, assign it to \cil{Y} and try to prove \cil{Goal}. \\
				On failure, select another number and retry.}
		\end{center}
	
		The ``retry'' semantics is implicit because of \prolog{}'s backtracking!
	}
	
}

%-------------------------------------------------------------------------------
\subsection{Partially-defined Structures}
%-------------------------------------------------------------------------------
\fr{The \cil{template/2} Predicate -- Try it Yourself}{
	\bl{Advices for \cil{template(+N:int, -Template:list)}}{
		We need this predicate to produce the partially-assigned \cil{Template = [(1, Y1), ..., (N, YN)]} assumed as second argument for the \cil{solution/2} predicate.
		\iz{
			\item One could think to map each element \cil{X} in the \emph{range} \cil{[1, ..., N]} into a couple \cil{(X, Yi)}, where \cil{Yi} is \emph{unbounded}
%			\item it should be trivial now
		}	
	}
	\ebl{\cil{template/2} usage examples}{
		\iz{
			\item \cil{?- template(1, L).} $\longrightarrow$ \cil{L = [(1,Y)]}.
			\item \cil{?- template(2, L).} $\longrightarrow$ \cil{L = [(1,Y1), (2,Y2)]}.
			\item \cil{?- template(4, L).} $\longrightarrow$ \cil{L = [(1,Y1), (2,Y2), (3,Y3),} \cil{(4,Y4)]}.
		}
	}

	\emph{Please note that \tuprolog{} may arbitrarly rename unbounded variables.}
}

\fr{The \cil{template/2} Predicate -- Possible Solution}{
	
	\begin{columns}
		\begin{column}{.62\linewidth}
			\ebl{Our proposal}{
				\lstinputlisting{./code/template.pl}
			}
		\end{column}
		\begin{column}{.36\linewidth}
			\iz{
				\item what if we replace the anonymous variable \cil{'_'} by a named variable \cil{Y}?
				\item what kind of template would we obtain?
				\item is the rules occurrence order relevant?
			}
		\end{column}
	\end{columns}

	\vspace{1cm}

	\emph{The important thing to note here is that partially-defined data structures can be easily defined in \prolog{} and are powerful to use.}
	
}

\fr{The \cil{n_queens/2} Predicate: Closing the Circle}{
	\begin{columns}
		\begin{column}{.40\linewidth}
			\ebl{Wiring up}{
				\lstinputlisting{./code/n_queens.pl}
			}
		\end{column}
	\end{columns}
	
	\bl{Ultimate tasks}{
		\en{
			\item try querying \cil{?- n_queens(8, S).}
			\iz{
				\item Enjoy ;)
			}
			\item now implement a solver for the $N$-Queens problem in Java.
		}
	}
}
