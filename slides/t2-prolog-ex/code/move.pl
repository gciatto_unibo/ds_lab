% move(+X, +Y, +S) 
move(X, Y, S) :- 
  turn(S),
  retract(cell(X, Y, e)), 
  asserta(cell(X, Y, S)),
  next_turn(S, NS),
  retract(turn(S)), 
  asserta(turn(NS)).
 
% next_turn(?S, ?S)
next_turn(x, o).
next_turn(o, x).