noattack((X1, Y1), (X2, Y2)) :-
  X1 =\= X2,  Y1 =\= Y2,   % not same col and not same row 
  (Y2 - Y1) =\= (X2 - X1), % not same diagonal
  (Y2 - Y1) =\= (X1 - X2). % not same anti-diagonal