lprintall([]).
lprintall([X | Xs]) :-
  write(X), nl,
  lprintall(Xs).