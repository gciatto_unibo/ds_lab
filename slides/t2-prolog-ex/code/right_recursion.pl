rprintall([]).
rprintall([X | Xs]) :-
  rprintall(Xs),
  write(X), nl.
	