noattack((X1, Y1), (X2, Y2)) :- !, % which is the purpose
  X1 =\= X2,  Y1 =\= Y2,           % of the cut here?
  (Y2 - Y1) =\= (X2 - X1),
  (Y2 - Y1) =\= (X1 - X2).
  
noattack(_, []).
noattack(Cell, [HeadCell | Others]) :- % where's recursion?
	noattack(Cell, HeadCell), % which one is called here?
	noattack(Cell, Others).   % which one is called here?