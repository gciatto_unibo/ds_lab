% is the match over?
?- not cell(_, _, e),   % Yes, if there's no empty cell
   aligned([S,S,S], _). % and 3 aligned symbols exist
% if yes and S = o then I'm winning

% if I can win, do it
?- (aligned([o,e,o], [_,cell(X,Y,e),_]); % semicolon = OR
   aligned([o,o,e], [_,_,cell(X,Y,e)])),
   move(X, Y, o).
   
% if I'm going to loose, avoid it
?- (aligned([x,e,x], [_,cell(X,Y,e),_]); % semicolon = OR
   aligned([x,x,e], [_,_,cell(X,Y,e)])),
   move(X, Y, o).
   
% ... other rules