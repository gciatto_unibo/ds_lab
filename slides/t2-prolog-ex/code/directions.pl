% north(?Cell1:tuple, ?Cell2:tuple)
north(cell(X,Y1,S1), cell(X,Y2,S2)) :- 
  cell(X, Y1, S1), % Why this subgoal?
  cell(X, Y2, S2), % Why this subgoal?
  1 is Y2 - Y1.
  
% north(?Cells:list)
north([C1, C2]) :- north(C1, C2).
north([C1, C2 | Cs]) :- 
  north(C1, C2), 
  north([C2 | Cs]). % Recursion's here
  
% line(?Cells:list)
line(Cells) :- north(Cells).

% Other directions are analogous
