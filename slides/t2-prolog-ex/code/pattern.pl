% pattern(?Cells, ?Symbols)
pattern([], []).
pattern([cell(_,_,S) | Cs], [S | Ss]) :- 
	pattern(Cs, Ss).