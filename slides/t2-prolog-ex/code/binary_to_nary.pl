binary_to_nary(Op) :-
  % op([X1, X2]) :- op(X1, X2).
  BaseCaseHead =.. [Op, [X, Y]],
  BaseCaseBody =.. [Op, X, Y],
  assertz(BaseCaseHead :- BaseCaseBody),
  % op([X1, X2 | Xs]) :- op(X1, X2), op([X2 | Xs]).
  RecursiveCaseHead =.. [Op, [X1, X2 | Xs]],
  RecursiveCaseBody1 =.. [Op, X1, X2],
  RecursiveCaseBody2 =.. [Op, [X2 | Xs]],
  assertz(RecursiveCaseHead :- (RecursiveCaseBody1, RecursiveCaseBody2)).
  
greater_than(X, Y) :- X > Y.

:- initialization(binary_to_nary(greater_than)).