% range(+F, +L, -R)
range(L, L, [L]).
range(F, L, [F | O]) :-
  F < L,
  N is F + 1,
  range(N, L, O).
  
% L -> Last
% F -> First
% O -> Others
% N -> Next
% R -> Range