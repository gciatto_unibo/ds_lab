% solution(+N:int, T:list)
solution(_, []).
solution(N, [(X, Y) | T]) :-
  solution(N, T),
  range(1, N, R), % R=[1..N]
  member(Y, R),
  noattack((X, Y), T).
  
% N -> num. of queens
% T -> template
% R -> range