template([], []).
template([X | Xs], [(X, _) | XYs]) :-
  template(Xs, XYs).
  
template(N, Template) :-
  range(1, N, Xs),
  template(Xs, Template).