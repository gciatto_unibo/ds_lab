greater_than(X, Y) :- X > Y.

:- initialization(binary_to_nary(greater_than)).