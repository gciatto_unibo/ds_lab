%===============================================================================
\section{Tic Tac Toe}
%===============================================================================
\newcommand{\tris}{Tic-Tac-Toe}

\fr{\prolog{} Example 2: \tris}{
	\bl{Brief description of the \tris{} game}{
		Well known 2-players turn-based discrete-world game.
		The players must locate their own \emph{symbols} (either \cil{'x'} or \cil{'o'}) over a $3 \times 3$ initially empty board.
		Once a symbol has been assigned to an empty cell, it cannot be removed.
		The first player \emph{aligning} 3 symbols of his/her is the winner.
		If the board is \emph{full} and no player has aligned 3 symbols, the match is \emph{even}.
	}
	\begin{exampleblock}{The state of a \tris{} match}
		The state of a \tris{} match consists of \iz{
			\item the next expected symbol
			\item the position of the already located symbols
		}
		These information can be represented in several ways.
		Each representation may influence the way one may \emph{reason} about the state of the match.
	\end{exampleblock}
}

\fr{Problems to be Faced}{
	\iz{
		\item several possible representation of the state
		\iz{
			\item should we only represent already located symbols?
			\item should we represent empty cells to?
			\item how to represent empty cells?
			\item why do representations differ?
		}
	
		\item state will eventually change due to players moves: unlike $N$-Queens, Tic Tac Toe is not pure computation $\rightarrow$ side effectes should be handled here
		\iz {
			\item how to deal with side effects and state changes?	
		}
	
		\item how can we exploit \prolog{} to reason about a Tic Tac Toe match in a declarative way?
		\iz {
			\item can we build artificial players?
			\item how difficult is it?	
		}
	}
}

%-------------------------------------------------------------------------------
\subsection{Knowledge Representation}
%-------------------------------------------------------------------------------
\fr{Representing State -- Possible Solution}{
	\label{slides.tris.state_repr}
%	\ebl{We could write ground facts within the theory}{
		\begin{columns}
			\begin{column}{.24\linewidth}
				\ebl{Our proposal}{
					\lstinputlisting{./code/ttt_cells.pl}
				}
			\end{column}
			\begin{column}{.74\linewidth}
				\iz{
					\item[!] the whole board is \emph{explicitly} represented
					
					\item[!] the ``next expected symbol'' (i.e. the turn) is represented by means of the \cil{turn/1} predicate
					
					\item[!] ``cross'' (\cil{'x'}), ``circle'' (\cil{'o'}) and ``empty'' (\cil{'e'}) symbols are represented as constants
					
					\vfill
					
					\item what do we mean by querying \cil{?- cell(X, Y, x).}?
					\iz{
						\item what by \cil{?- cell(X, Y, e).}?
						\item what by \cil{?- cell(2, 3, S).}?
						\item what by \cil{?- cell(X, Y, S).}?
					}
				
					\item this is just an example: initially, all cells should be empty
				}
			\end{column}
		\end{columns}
%	}
}

\lfr{Representing State -- Alternative Solutions}{
	\bl{Representing all cells, empty symbol with \cil{'_'}}{
		\iz{
			\item[e.g.] \cil{cell(1, 2, _). cell(2, 1, _). \%...}
			\hfill
			\item how would you check if a particual cell is empty?
			\item how would you check if a particual cell contains symbol \cil{S}?
			\item how would you iterate over empty (resp. non-empty) cells?
		}
	}

	\bl{Representing only non-empty cells}{
		\iz{
			\item[e.g.] \cil{cell(1, 1, o). cell(1, 3, x). cell(2, 2, x).}
			\hfill
			\item same questions as above.
			\iz {\item same answers?}
		}
	}

	\abl{Is keeping track of the turn really necessary?}{
		We could assume the \emph{initual} turn to always be \cil{'x'}.
		We could then compute the turn by counting the amount of non-empty cells\footnote{even number $\implies$ \cil{'x'}, odd number $\implies$ \cil{'o'}}.
		\iz {
			\item would this computation be affected by the chosen representation of the board?
			\item how?
		}
	}
}

%-------------------------------------------------------------------------------
\subsection{Dealing with Side-Effects}
%-------------------------------------------------------------------------------
\lfr{Changing State Dynamically}{
	\iz{
		\item logic atoms are immutable
		\iz {
			\item \cil{cell(1, 2, e)} is an axiom $\implies$ cell \cil{(X, Y)} is empty --- now and forever --- according to 1OL semantics 
		}
	
		\hfill
		
		\item[$\times$] this is not true in practice since the sentence ``cell \cil{(X, Y)} is empty'' may eventually turn from false to true after some player move
		
		\hfill
		
		\item[$\rightarrow$] \prolog{} allows the program to \emph{dynamically} update its \emph{Knoledge Base} (KB) during its own execution
		\iz{
			\item KB $\approx$ Theory + Dynamically added facts and rules
		}
	
		\hfill
		
		\item if player 2 succeeds in moving \cil{'o'} to cell \cil{(1, 2)}, then we should
		\iz{
			\item \emph{retract} our knowledge about the \emph{fact} \cil{cell(1, 2, e)}
			\item \emph{asserting} that \cil{cell(1, 2, o)} instead.
		} 
	}
}

\fr{The \cil{assert/1} and \cil{retract/1} Predicates}{
	
	\bl{Adding clauses to the KB}{
		\iz{
			\item \cil{assertz(Klause)}, or simply \cil{assert(Klause)}, evaluate to \cil{true}, with the side effect that the clause \cil{Klause} is appended \alert{at the end} of the KB
			\item \cil{asserta(Klause)} evaluates to \cil{true}, with the side effect that the clause \cil{Klause} is added \alert{to the beginning} of the KB
			\item[!] why do we need to choose where to insert clauses?
		}
	}

	\bl{Removing clauses from the KB}{
		\iz{
			\item \cil{retract(Klause)} evaluates to \cil{true}, with the side effect of removing from the KB a clause that matches \cil{Klause} (which must be at least partially instantiated); multiple solutions are given upon backtracking
		}
	}
}

\lfr{The \cil{move/3} Predicate -- Try it Yourself}{
	\bl{Advices for \cil{move(+X:int, +Y:int, +S:const)}}{
		This predicate is used by players to make their moves.
		It moves symbol \cil{S} into \cil{(X, Y)}, after ensuring that
		\iz{
			\item the next expected symbol is \cil{S}
			\item cell \cil{(X, Y)} is empty 
		}
		If the operation succeeds, the next expected symbol is updated accordingly
		
		\iz{
			\item[!] you may need to write some support predicate
		}
	}

	\ebl{\cil{move/3} use cases}{
		Suppose the KB coincides with the state representation shown in slide \ref{slides.tris.state_repr}.
		\iz{
			\item[!] \alert{you should reload your theory after each test in order to revert the side-effects.}
			\vfill
			\item[?-] \cil{move(2, 2, o).} $\longrightarrow$ \cil{no.}
			\item[?-] \cil{move(1, 1, o).} $\longrightarrow$ \cil{no.}
			\item[?-] \cil{move(3, 1, o).} $\longrightarrow$ \cil{yes.}
			\item[?-] \cil{move(3, 1, o), move(3, 1, o).} $\longrightarrow$ \cil{no.}
			\item[?-] \cil{move(3, 1, o), move(3, 1, o).} $\longrightarrow$ \cil{no.}
			\iz{
				\item (without reverting side effects) \cil{?- move(2, 1, x).} $\longrightarrow$ \cil{yes.}
			}
		}
	}
}
\fr{The \cil{move/3} Predicate -- Possible Solution}{
	\begin{columns}
		\begin{column}{.44\linewidth}
			\ebl{Our proposal}{
				\lstinputlisting{./code/move.pl}
			}
		\end{column}
		\begin{column}{.54\linewidth}
			\iz{
				\item where are we checking if \cil{S} is the next expected turn?
				\item where are we checking if \cil{(X, Y)} is empty?
				\item try replacing \cil{asserta/1} with \cil{assertz/1}: is it different in this case?
				\iz{
					\item is it different in the general case?
				}
				\item is the order of subgoals in \cil{move/3} relevant, and why?
				\item what is the purpose of \cil{next\_turn/2}?
			}
		\end{column}
	\end{columns}
}
%-------------------------------------------------------------------------------
\subsection{\prolog{} and AI}
%-------------------------------------------------------------------------------
\lfr{Drafting the \cil{aligned/2} Predicate}{
	\bl{Suppose \cil{aligned/2} is available, having the following semantics:}{
		\cil{aligned(+Symbols:list, ?Cells:list)}
		\iz{
			\item \cil{Symbols} is an \emph{input} list of symbols
			\iz{\item e.g. \cil{[x, x, e]}}
			\item \cil{Cells} is an \emph{output} list of cells
			\iz{\item e.g. \cil{[cell(1,3,x), cell(2,2,x), cell(3,1,e)]}}
			\item the predicate searches a \emph{sequence of cells} whose symbols match the input sequence of symbols, \emph{regardless of their orientation} on the board
		}
	}
	
	\ebl{\cil{aligned/2} usage examples}{
		Suppose the KB coincides with the state representation shown in slide \ref{slides.tris.state_repr}.
		\iz{
			\item \alert{you should reload your theory after each test in order to revert the side-effects.}
			\vfill
			\item[?-] \cil{aligned([o, e, e], L).} $\longrightarrow$ \cil{L/[cell(1,1,o),} \cil{cell(2,1,e),} \cil{cell(3,1,e)]}
			\item[?-] \cil{aligned([e, e, e], L).} $\longrightarrow$ \cil{L/[cell(3,3,e),} \cil{cell(3,2,e),} \cil{cell(3,1,e)] ;} \cil{L/[cell(3,1,e),} \cil{cell(3,2,e),} \cil{cell(3,3,e)]}
			\item[?-] \cil{aligned([S, S, S], _), S \\== e.} $\longrightarrow$ \cil{no.}
		}
	}

	\framebreak
	
	
	\alert{Why is \cil{aligned/2} so useful?}
	\iz {
		\item we do not need to reason on coordinates anymore
		\item we can easily write orientation-independent rules
		\item we can write the \emph{artificial intelligence} (AI) of a \tris{} player simply combining the \cil{aligned/2} and \cil{move/3} predicates
		\vspace{1cm}
		\item[e.g.] let us pretend to be the AI driving player 2 moves with symbol \cil{'o'}
	}

	\framebreak
	
	\ebl{AI of player 2 (\cil{'o'})}{
		\label{slides.tris.no_empty_cell}
		\lstinputlisting{./code/ai1.pl}
	}
}

\fr{The \cil{aligned/2} Predicate -- Try it Yourself}{
	\bl{Advices for the \cil{aligned/2} predicate}{
		\iz {
			\item we need to check if some cell is \alert{northward} to another
			\iz {
				\item and if each cell \emph{in a list} is \alert{northward} to \emph{the next one} 
				\item[!] note that Y-axis progresses up-down
			}
			\item \ldots
			\item we actually need to check this for \alert{all cardinal directions}
			\iz {\item[i.e.] north, south, east, west, northeast, northwest, southeast, southwest}
			\item we need a way to transform a sequence of symbols \cil{[S1, S2, ...]} into a sequence of partially-assigned cells \cil{[cell(X1,Y1,S1),} \cil{cell(X2,Y2,S2), ...]}
			\item finally, cells \emph{in a list} are \alert{aligned} if \emph{there exists a direction} such that all cells are on that direction
		}
	}
}

%-------------------------------------------------------------------------------
\subsection{Reasoning about (Logic) space}
%-------------------------------------------------------------------------------
\lfr{The \cil{aligned/2} Predicate -- Possible Solution}{
	\label{slies.tris.north}
	\begin{columns}
		\begin{column}{.64\linewidth}
			\ebl{Some utilities (repeat for each direction)}{
				\lstinputlisting{./code/directions.pl}
			}
		\end{column}
		\begin{column}{.34\linewidth}
			\iz{
				\item why there's no need for \cil{X1} and \cil{X2} in \cil{north/2}?
				\item what's the purpose of \cil{cell(X,Y1,S1)}  \& \cil{cell(X,Y2,S2)}?
				\item why not \cil{Y2 - Y1 is 1}?
				\item what's the purpose of \cil{north/1}?
				\item what's the purpose of \cil{line/1}?
			}
		\end{column}
	\end{columns}

	\framebreak
	
	\begin{columns}
		\begin{column}{.69\linewidth}
			\ebl{From symbols lists to cells lists (and vice versa)}{
				\lstinputlisting{./code/pattern.pl}
			}
			\begin{columns}
				\begin{column}{.65\linewidth}
					\ebl{Wiring up}{
						\lstinputlisting{./code/aligned.pl}
					}
				\end{column}
			\end{columns}
		\end{column}
		\begin{column}{.29\linewidth}
			\iz {
				\item is \cil{pattern/2} generating lists of symbols from lists of cells or vice versa, and why?
				\vspace{.5cm}
				\item what is the difference if we switch \cil{line} and \cil{pattern} subgoals?
			}
		\end{column}
	\end{columns}
}

%-------------------------------------------------------------------------------
\subsection{CWA Implications}
%-------------------------------------------------------------------------------
\lfr{Consequences of the Closed World Assumption}{
	\bl{Closed World Assumption (CWA)}{
		\begin{center}
			\emph{What is not known to be true, is false.}
		\end{center}
		\vfill
		\iz {
			\item this is the underlying assumption in \prolog{}
			\item[$\rightarrow$] whatever cannot be inferred from the KB is evalued to \cil{false}.
		}
	}

	\framebreak

	\ebl{Negation as failure (NaF): \cil{not Goal}}{
		Remember subgoal \cil{not cell(\_,\_,e)} from the AI example?
		\iz {
			\item it succeeds if the engine fails at proving \cil{cell(\_,\_,e)}
			\item \ldots{} which requires visiting the whole proof-tree of \cil{?-cell(\_,\_,e).}
			\iz{\item it might become quite inefficient}
		}
	}
	
	\begin{columns}
		\begin{column}{.55\linewidth}
			\bl{NaF: possible implementation}{
				\lstinputlisting{./code/not.pl}
			}
		\end{column}
	\end{columns}

	\abl{Not everything can be inferred}{
		Remember the \cil{north/2} predicate in slide \ref{slies.tris.north}?
		\iz {
			\item it can be used in a ``generative'' way
			\iz{
				\item[e.g.] \cil{?- north(C, cell(2, 2, _)).} meaning ``what's the cell \cil{C} northward to \cil{(2,2)}?''
				\item[e.g.] \cil{?- north(C1, C2).} meaning ``let \cil{C1} and \cil{C2} be two cells such that \cil{C1} is northward to \cil{C2}''
			}
			\item try these queries after removig the \cil{cell(X,Y1,S1)} \& \cil{cell(X,Y2,S2)} subgoals from \cil{north/2}. What changes?
			\iz{\item try drawing the prooth-tree for both versions of \cil{north/2}}
		}
	}
}

%-------------------------------------------------------------------------------
\subsection{Meta-Programming}
%-------------------------------------------------------------------------------
\lfr{The Need for Factorisation}{
	Remember the directions code from slide \ref{slies.tris.north}?
	\iz {
		\item some predicates share the same structure and simply differ for their \emph{functors}
		\iz{\item[e.g.] \cil{north/1}, \cil{east/1}, \cil{south\_west/1}, \ldots}
		\item writing them was repetitive and boring
	}
	\vspace{.7cm}
	We would need a way to factorise predicates having the same logic
	\iz {
		\item sadly, only constants can be used as functors
		\iz{\item[i.e.] we \alert{cannot} write something like \cil{Op([C1, C2 | Cs]) :- Op(C1, C2), Op([C2 | Cs]).}}
		\item how could we ``parametrize'' the functor of some predicate?
	}

	\framebreak

	\bl{The power of building terms}{
		\iz {
			\item suppose we were able to \alert{dynamically} create a term \cil{T} (e.g. \cil{T = a(X)})
			\iz{\item we could then add facts/rules to our theory by means of \cil{assert(T)}}
			\vspace{.5cm}
			\item suppose also we were able to call \cil{assert(T)} on theory \alert{loading}
			\iz{\item we could then \alert{generate} facts/rules programmatically, \emph{without the need of typing them}}
		}
	}
}

\fr{Building Terms Dynamically}{

	\bl{The \cil{'=..'(?Term:any, ?List:list)} operator definition}{
			\begin{center}
				\textit{\cil{Term =.. List} is true if \cil{List} is a list consisting of the \emph{functor} and all arguments of \cil{Term}, in this order.}
			\end{center}
	}

	\ebl{Examples for the \cil{'=..'/2} operator}{
		\iz{
			\item[?-] \cil{a(b, c, d) =.. [a, b, c, d].} $\longrightarrow$ \cil{yes.}
			\item[?-] \cil{a([X|Y], [A|B]) =.. [a, [X|Y], [A|B]].} $\longrightarrow$ \cil{yes.}	
			\item[?-] \cil{X =.. [a, b].} $\longrightarrow$ \cil{X/a(b).}	
			\item[?-] \cil{B =.. [b, c], X =.. [a, B].} $\longrightarrow$ \cil{X/a(b(c)).}
		}
	}
}

\fr{Directives}{
	
	\bl{\prolog{} directives syntax}{
		\begin{center}
			\textit{[\tuprolog{}] engines support natively some directives, that can be defined by means of the \cil{:-/1} predicate in theory specification. 
			Directives are used to specify properties of clauses and engines, format and syntax
			of terms.}
		\end{center}
	}
	
	\ebl{The \cil{initialization/1} directive}{
%		\iz{
			\cil{:- initialization(Goal).}
			\iz {
				\item sets the goal to be executed just after the theory has been loaded
			}
%		}
	}

	\ebl{The \cil{include/1} directive}{
%		\iz{
			\cil{:- include(FileName).}
			\iz {
				\item loads the theory contained in the file specified by \cil{FileName}
			}
%		}
	}
}

\lfr{Meta-Programming}{
	\bl{Informal definition}{
		\begin{center}
			\textit{Meta-programming is a programming technique in which computer programs have the ability to treat programs as their data, possibly by manipulating their own code \alert{while running}}
		\end{center}
	
		\iz{
			\item this is made easy in \prolog{}, where both the program and its data share the same syntax
		}
	}

	\framebreak

	\ebl{Meta-programming example -- The \cil{binary_to_nary/1} predicate}{
		\lstinputlisting{./code/binary_to_nary.pl}
	}

	\iz{
		\item \cil{greater_than/2} is a binary predicate
		\iz{\item the \cil{greater_than/1} version, accepting an arbitrarily long list of numbers is attained by means of meta-programming}
		\vspace{1cm}
		\item try the following queries, with \alert{and without} the \cil{initialization/1} directive
		\iz{
			\item[?-] \cil{greater_than(3, 2).}
			\item[?-] \cil{greater_than([4, 3, 2, 1]).}
		}
		\vspace{.4cm}
		\item try to make the \tris{} code more concise by means of the meta-programming technique.
		\iz{\item a possible solution is available on the course site}
	}
}
