agent_execution :- % ... agent_loop ...
  
agent_loop :- 
  divine_comedy(Lines), % <-- get a list of verses
  partition(Lines, 3, Tasks), % <-- group verses into triplets
  schedule_tasks(Tasks), 
  length(Tasks, N),
  aggregate_results(N, Result).

schedule_tasks([]).
schedule_tasks([T | Ts]) :-
  out(task(count_vowels, T)), % <-- publish the task
  schedule_tasks(Ts).
  
aggregate_results(N, R) :- aggregate_results(N, R, 0).
aggregate_results(0, R, R).
aggregate_results(N, R, Accumulator) :-
  in(result(count_vowels, Lines, Partial)), % <-- retrieve the partial result
  Accumulator1 is Accumulator + Partial, N1 is N - 1,
  aggregate_results(N1, R, Accumulator1).