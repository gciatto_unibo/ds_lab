agent_name(responder).  % TODO insert responder_<surname> here  
node_address('localhost').    node_port(20504).

agent_execution :-
  agent_name(MyName), node_address(Address), node_port(Port),
  acquire_acc(MyName, Address, Port),
  see(stdin), % <-- open standard input
  (agent_loop; true), % <-- PAY ATTENTION HERE
  seen(stdin), % <-- close standard input
  release_acc, !.

agent_loop :- !, agent_loop_step, agent_loop. % <-- recursion

agent_loop_step :-
  agent_name(MyName),
  in(message(MyName, Payload, Sender)),
  read(Response), % <-- read a dot-terminated term from standard input
  out(message(Sender, Response, MyName)).