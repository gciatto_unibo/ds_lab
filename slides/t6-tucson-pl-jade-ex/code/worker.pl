agent_execution :- % ... agent_loop ...
  
agent_loop :- !, agent_loop_step, agent_loop. % <-- recursion
  
agent_loop_step :-
  in(task(Name, Args)), % <-- retrieve a task
  handle_request(Name, Args).

% Handle the known task "count_vowels"
handle_request(count_vowels, Lines) :-
  count_vowels(Lines, R),
  out(result(count_vowels, Lines, R)). % <-- publish partial result

% Handle unknown tasks
handle_request(Name, Args) :- out(task(Name, Args)).
  

