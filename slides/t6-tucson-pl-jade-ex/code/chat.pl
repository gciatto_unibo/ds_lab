agent_name($agent_name). % TODO insert <surname>_<name> here
node_address('localhost').    node_port(20504).

send_message(Message, Receiver) :-
  agent_name(Me),
  out(message(Receiver, Message, Me)).

receive_message(Message, Sender) :-
  agent_name(Me),
  in(message(Me, Message, Sender)).