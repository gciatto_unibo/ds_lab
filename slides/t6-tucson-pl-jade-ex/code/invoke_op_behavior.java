public class InvokeTucsonOperation extends Behaviour {

	private final BridgeToTucson bridge;
    private final AbstractTucsonOrdinaryAction operation;
    private TucsonOpCompletionEvent result;

    @Override
    public final void action() {
        result = bridge.synchronousInvocation(op, null, this);

        if (result != null) {
            handleResult(result);
        } else {
            block();
        }
    }

    @Override
    public final boolean done() {
        return result != null;
    }

}